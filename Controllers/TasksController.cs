﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace SimpleWebAppMVC.Controllers
{
    /**
     * Tasks Controller
     */
    public class TasksController : Controller
    {
        //private readonly AppDbContext dbContext;

        /**
         * TasksController constructor.
         * @param dbCtx Application database context
         */
        public TasksController()
        {
           // this.dbContext = dbCtx;
        }

        /**
         * GET: /Tasks/Create
         */
        [HttpGet]
        public IActionResult Create()
        {
            return View(new Models.Task());
        }

            }
}
